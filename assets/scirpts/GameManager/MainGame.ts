import SelectNodeManager from "./SelectNodeManger";
import Item1Manager from "./Item1Manager";
import Item2Manager from "./Item2Manager";
import Item3Manager from "./Item3Manager";
import ShowNotice from "./ShowNotice";
import ShowCorrectItem from "./ShowCorrectItem";
import ShowPosition from "./ShowPosition";
import AddNode1 from "./AddNode1";
import AddNode2 from "./AddNode2";
import Global from "../Utils/Global"

const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {
    @property(SelectNodeManager)
    Node1: SelectNodeManager = null;
    @property(SelectNodeManager)
    Node2: SelectNodeManager = null;

    @property(Item1Manager)
    Item1: Item1Manager = null;
    @property(Item2Manager)
    Item2: Item2Manager = null;

    @property(ShowPosition)
    PosNode: ShowPosition = null;

    checkPositionItem1: number = 0;
    checkPositionItem2: number = 0;
    checkItem: number = 1;
    isChooseItem: boolean = false;

    @property(ShowNotice)
    noticeNode: ShowNotice = null;

    @property(cc.Node)
    EndNode: cc.Node = null;
    @property(cc.Node)
    excellentNode: cc.Node = null;
    @property(cc.Node)
    Notice: cc.Node = null;

    numberControl: number = 0;

    @property({ type: cc.AudioClip })
    clickButton: cc.AudioClip = null;
    @property({ type: cc.AudioClip })
    bgSound: cc.AudioClip = null;
    @property({ type: cc.AudioClip })
    endSound: cc.AudioClip = null;
    // @property(cc.Node)
    // NodeActiveEnd: cc.Node = null;
    @property(cc.Node)
    NodeInstall: cc.Node = null;

    @property(cc.Node)
    DoneItem11: cc.Node = null;
    @property(cc.Node)
    DoneItem12: cc.Node = null;
    @property(cc.Node)
    DoneItem13: cc.Node = null;

    @property(cc.Node)
    DoneItem21: cc.Node = null;
    @property(cc.Node)
    DoneItem22: cc.Node = null;
    @property(cc.Node)
    DoneItem23: cc.Node = null;

    // @property(cc.Animation)
    // anims: cc.Animation = null;

    @property(cc.Node)
    node11: cc.Node = null;
    @property(cc.Node)
    node12: cc.Node = null;
    @property(cc.Node)
    node13: cc.Node = null;

    @property(cc.Node)
    node21: cc.Node = null;
    @property(cc.Node)
    node22: cc.Node = null;
    @property(cc.Node)
    node23: cc.Node = null;

    @property(AddNode1)
    AddNode1: (AddNode1) = null;

    @property(AddNode2)
    AddNode2: (AddNode2) = null;

    //responsive
    @property(cc.Node)
    bg: cc.Node = null;
    @property(cc.Node)
    pos1: cc.Node = null;
    @property(cc.Node)
    pos2: cc.Node = null;
    changeRotation: number = 0;
    @property(cc.Node)
    node1: cc.Node = null;
    @property(cc.Node)
    node2: cc.Node = null;

    @property(cc.Node)
    logo: cc.Node = null;
    @property(cc.Node)
    installGg: cc.Node = null;
    @property(cc.Node)
    menu1: cc.Node = null;
    @property(cc.Node)
    menu2: cc.Node = null;
    @property(cc.Node)
    praise: cc.Node = null;

    @property(cc.Node)
    startGuide: cc.Node = null;
    @property(cc.Node)
    wrongNode: cc.Node = null;
    @property(cc.Animation)
    effectDel: cc.Animation = null;
    //  declare global{
    //     interface Window { Insall:any;}
    // }
    // window.Insall = window.Insall || {};
    onLoad() {
        this.changeRotation = this.node.width;
        this.bg.scale = this.node.width / 920;
        this.pos1.scale = this.node.width / 960;
        this.pos2.scale = this.node.width / 960;
        this.node1.scale = this.node.width / 960;
        this.node2.scale = this.node.width / 960;
        this.EndNode.scale = this.node.width / 980;
        this.wrongNode.scale = this.node.width / 1300;
        this.startGuide.scale = this.node.width / 960;
        //
        this.logo.scale = this.node.width / 1720;
        this.installGg.scale = this.node.width / 960;
        this.menu1.scale = this.node.width / 960;
        this.menu2.scale = this.node.width / 960;
        this.Notice.scale = this.node.width / 960;
        this.praise.scale = this.node.width / 960;

    }
    update() {
        if (this.changeRotation != this.node.width) {
            this.bg.scale = this.node.width / 920;
            this.pos1.scale = this.node.width / 960;
            this.pos2.scale = this.node.width / 960;
            this.node1.scale = this.node.width / 960;
            this.node2.scale = this.node.width / 960;
            this.EndNode.scale = this.node.width / 980;
            this.wrongNode.scale = this.node.width / 1300;
            this.startGuide.scale = this.node.width / 960;
            //
            this.logo.scale = this.node.width / 1720;
            this.installGg.scale = this.node.width / 960;
            this.menu1.scale = this.node.width / 960;
            this.menu2.scale = this.node.width / 960;
            this.Notice.scale = this.node.width / 960;
            this.praise.scale = this.node.width / 960;
        }
        this.changeRotation = this.node.width;
    }
    start() {
        this.scheduleOnce(() => {
            this.startGuide.opacity = 0;
        }, 1.9);
        this.scheduleOnce(() => {
            this.effectDel.node.opacity = 255;
            this.effectDel.play();
        }, 2.5);
        this.scheduleOnce(() => {
            this.wrongNode.opacity = 0;
        }, 3.3);
        // (<any>window).gameReady();
        cc.audioEngine.play(this.bgSound, true, 1);
        this.scheduleOnce(() => {
            this.noticeNode.appear();
        }, 3.5);
        this.scheduleOnce(() => {
            this.Node1.appear();
            this.PosNode.showPosItem1();
        }, 3.8);
    }

    // playAnimation() {
    //     this.anims.play();
    // }
    //show icon DoneItem, disappear notice text, run sound click, if not choose item , u can't touch positon for move
    ShowOther() {
        cc.audioEngine.play(this.clickButton, false, 1);
        // this.playAnimation();
        this.noticeNode.disappear();  //hide notice txt
        this.isChooseItem = true;
    }

    HideAllHand() {
        Global.isHideHand = true;
    }
    ActiveAllHand() {
        Global.isHideHand = false;
    }
    //3 button Choose of Item 1
    choose11() {
        this.HideAllHand();
        this.AddNode1.hideNode();
        this.Item1.hide();
        this.ShowOther();
        this.Item1.item12.node.active = false;
        this.Item1.item13.node.active = false;
        this.Item1.item11.node.active = true;
        this.DoneItem12.active = false;
        this.DoneItem13.active = false;
        this.DoneItem11.active = true; //show item done 11
        this.PosNode.hidePosItem1();
        this.Item1.chooseItem11();  //put item 11
        this.node12.opacity = 255;
        this.node13.opacity = 255;
        this.node11.opacity = 0;    //hide item 11 on menu
    }

    Done11() {
        if (this.DoneItem11.active) {
            this.Node1.selectedNode(); //hide menu select 
            this.DoneItem11.active = false;
            cc.audioEngine.play(this.clickButton, false, 1);
            if (this.isChooseItem) {
                this.showChooseItem2();
            }
        }
    }

    choose12() {
        this.HideAllHand();
        this.AddNode1.hideNode();
        this.Item1.hide();
        this.ShowOther();
        this.Item1.item11.node.active = false; //hide item 11 used
        this.Item1.item12.node.active = true;
        this.Item1.item13.node.active = false;
        this.DoneItem11.active = false; // hide item done used
        this.DoneItem13.active = false;
        this.DoneItem12.active = true;
        this.Item1.chooseItem12();  // put item 12
        this.PosNode.hidePosItem1();
        this.node11.opacity = 255; // show  again item 11 on menu
        this.node12.opacity = 0;    //hide item 12 on menu
        this.node13.opacity = 255;

    }
    Done12() {
        if (this.DoneItem12.active) {
            this.Node1.selectedNode(); //hide menu select 
            this.DoneItem12.active = false;
            cc.audioEngine.play(this.clickButton, false, 1);
            if (this.isChooseItem) {
                this.showChooseItem2();
            }
        }
    }
    choose13() {
        this.HideAllHand();
        this.AddNode1.hideNode();
        this.Item1.hide();
        this.ShowOther();
        this.Item1.item11.node.active = false;
        this.Item1.item12.node.active = false;
        this.Item1.item13.node.active = true;
        this.DoneItem11.active = false; // hide item done used
        this.DoneItem12.active = false;
        this.DoneItem13.active = true;
        this.Item1.chooseItem13();
        this.PosNode.hidePosItem1();
        this.node11.opacity = 255;
        this.node12.opacity = 255;
        this.node13.opacity = 0;

    }

    Done13() {
        if (this.DoneItem13.active) {
            this.Node1.selectedNode(); //hide menu select 
            this.DoneItem13.active = false;
            cc.audioEngine.play(this.clickButton, false, 1);
            if (this.isChooseItem) {
                this.showChooseItem2();
            }
        }
    }

    //3 button Choose of Item 2
    choose21() {
        this.HideAllHand();
        this.AddNode2.hideNode();
        this.Item2.hide();
        this.ShowOther();
        this.Item2.item22.node.active = false;
        this.Item2.item23.node.active = false;
        this.Item2.item21.node.active = true;
        this.DoneItem22.active = false;
        this.DoneItem23.active = false;
        this.DoneItem21.active = true; //show item done 11
        this.Item2.chooseItem21();
        this.PosNode.hidePosItem2();
        this.node22.opacity = 255;
        this.node23.opacity = 255;
        this.node21.opacity = 0;    //hide item 11 on menu
    }

    Done21() {
        if (this.DoneItem21.active) {
            this.Node2.selectedNode(); //hide menu select 
            this.DoneItem21.active = false;
            cc.audioEngine.play(this.clickButton, false, 1);
            if (this.isChooseItem) {
                // this.DeletePoint2();
                //Active all node end
                // this.NodeActiveEnd.active = true;
                setTimeout(() => {
                    this.showEndGame();
                }, 1300);
            }
        }
    }

    choose22() {
        this.HideAllHand();
        this.AddNode2.hideNode();
        this.Item2.hide();
        this.ShowOther();
        this.Item2.item21.node.active = false;
        this.Item2.item23.node.active = false;
        this.Item2.item22.node.active = true;
        this.DoneItem21.active = false;
        this.DoneItem23.active = false;
        this.DoneItem22.active = true;
        this.Item2.chooseItem22();
        this.PosNode.hidePosItem2();
        this.node21.opacity = 255;
        this.node23.opacity = 255;
        this.node22.opacity = 0;
    }

    Done22() {
        if (this.DoneItem22.active) {
            this.Node2.selectedNode(); //hide menu select 
            this.DoneItem22.active = false;
            cc.audioEngine.play(this.clickButton, false, 1);
            if (this.isChooseItem) {
                //Active all node end
                // this.NodeActiveEnd.active = true;
                setTimeout(() => {
                    this.showEndGame();
                }, 1300);
            }
        }
    }

    choose23() {
        this.HideAllHand();
        this.AddNode2.hideNode();
        this.Item2.hide();
        this.ShowOther();
        this.Item2.item21.node.active = false;
        this.Item2.item22.node.active = false;
        this.Item2.item23.node.active = true;
        this.DoneItem21.active = false; // hide item done used
        this.DoneItem23.active = true;
        this.DoneItem22.active = false;
        this.Item2.chooseItem23();
        this.PosNode.hidePosItem2();
        this.node21.opacity = 255;
        this.node22.opacity = 255;
        this.node23.opacity = 0;
    }

    Done23() {
        if (this.DoneItem23.active) {
            this.Node2.selectedNode(); //hide menu select 
            this.DoneItem23.active = false;
            cc.audioEngine.play(this.clickButton, false, 1);
            if (this.isChooseItem) {
                //Active all node end
                // this.NodeActiveEnd.active = true;
                setTimeout(() => {
                    this.showEndGame();
                }, 1000);
            }
        }
    }

    showChooseItem2() {
        this.ActiveAllHand();
        this.isChooseItem = false;
        this.checkItem++;
        this.noticeNode.setText("Item2_txt_kr");
        this.noticeNode.appear();
        this.scheduleOnce(() => {
            this.PosNode.showPosItem2();
            this.Node2.appear();
        }, 0.3);
    }

    showEndGame() {
        // (<any>window).gameEnd();
        cc.audioEngine.play(this.endSound, false, 1);
        this.NodeInstall.opacity = 0;
        this.Notice.opacity = 0;
        this.excellentNode.active = true;
        setTimeout(() => {
            this.EndNode.active = true;
            this.excellentNode.opacity = 0;
        }, 2000);

    }

}
