const { ccclass, property } = cc._decorator;

@ccclass
export default class Item3 extends cc.Component {

    @property(cc.Animation)
    anims: cc.Animation = null;

    playAnimation() {
        this.anims.play();
    }
}
