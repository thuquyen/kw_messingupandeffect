
const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Animation)
    add1: cc.Animation = null;

    @property(cc.Animation)
    add2: cc.Animation = null;

    @property(cc.Animation)
    add3: cc.Animation = null;

    @property(Number)
    delayTime: number = 0.1;

    @property(cc.Animation)
    appearEff: cc.Animation[] = [];

    playAnimation() {
        for (let i = 0; i < this.appearEff.length; i++) {
            this.appearEff[i].node.active = true;
        }

        //other
        this.scheduleOnce(() => {
            this.add1.play();
            this.add2.play();
            this.add3.play();
            for (let i = 0; i < this.appearEff.length; i++) {
                this.appearEff[i].play();
            }
        }, this.delayTime * 3)
    }
    hideNode() {
        this.add1.node.opacity = 0;
        this.add2.node.opacity = 0;
        this.add3.node.opacity = 0;
    }
    // update (dt) {}
}
