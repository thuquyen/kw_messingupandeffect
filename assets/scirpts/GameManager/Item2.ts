import AddNode2 from "./AddNode2";
const { ccclass, property } = cc._decorator;

@ccclass
export default class Item2 extends cc.Component {

    @property(cc.Animation)
    anims: cc.Animation = null;
    @property(AddNode2)
    AddNode2: AddNode2 = null;
    @property(cc.Animation)
    AppearEff: cc.Animation = null;

    playAnimation() {
        this.AppearEff.node.active = true;
        this.anims.play();
        this.AppearEff.play();
        this.scheduleOnce(() => {
            this.AddNode2.playAnimation();
        }, 0.4);
    }
    hideNode() {
        this.anims.node.opacity = 0;
    }
}
