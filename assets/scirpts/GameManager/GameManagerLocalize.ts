import SelectNodeManager from "./SelectNodeManger";
import CouchManager from "./CouchManager";
import LampManager from "./LampManager";
import WallDecorManager from "./WallDecorManager";
import NoticeManagerLocalize from "./NoticeManagerLocalize";
import EndGame from "./EndGame";


const { ccclass, property } = cc._decorator;

@ccclass
export default class GameManagerLocalize extends cc.Component {
    @property(cc.Node)
    selectCouchNode: cc.Node = null;
    @property(SelectNodeManager)
    couchNode: SelectNodeManager = null;
    @property(cc.Node)
    wallDecor: cc.Node = null;
    @property(SelectNodeManager)
    wallDecorNode: SelectNodeManager = null;
    @property(cc.Node)
    lamp: cc.Node = null;
    @property(SelectNodeManager)
    lampNode: SelectNodeManager = null;
    @property(cc.Node)
    selectWallNode: cc.Node = null;
    @property(cc.Node)
    selectLampNode: cc.Node = null;
    @property(CouchManager)
    couchManager: CouchManager = null;
    @property(LampManager)
    lampManager: LampManager = null;
    @property(WallDecorManager)
    wallDecorManager: WallDecorManager = null;
    @property(LampManager)
    lampManager1: LampManager = null;
    // @property(WallDecorManager)
    // wallDecorManager1: WallDecorManager = null;
    @property(NoticeManagerLocalize)
    noticeNode: NoticeManagerLocalize = null;
    @property(cc.Node)
    endGameNode: cc.Node = null;
    @property(EndGame)
    endGameAni: EndGame = null;
    // @property(cc.Node)
    // btnInstall: cc.Node = null;
    // @property({ type: cc.AudioClip })
    // clickButton: cc.AudioClip = null;
    // @property({ type: cc.AudioClip })
    // bgSound: cc.AudioClip = null;
    // @property()
    // isEndGame: Boolean = false;
    // @property({type:cc.AudioClip})
    // endSound: cc.AudioClip = null;
    @property(cc.Node)
    private itemTree: cc.Node = null
    @property(cc.Node)
    private p1: cc.Node = null
    start() {
        // cc.audioEngine.play(this.bgSound, true, 1);
        this.scheduleOnce(() => {
            this.noticeNode.appear();
        }, 0.1);
        this.scheduleOnce(() => {
            this.couchNode.appear();
        }, 0.3);
    }

    DeleteItem() {
        this.itemTree.destroy();
    }
    MoveItem(){
        this.itemTree.setPosition(this.p1.getPosition().x,this.p1.getPosition().y);
    }

    chooseCouch1() {
        // cc.audioEngine.play(this.clickButton, false, 1);
        this.couchManager.chooseCouch1();
        this.couchNode.selectedNode();
        this.noticeNode.disappear();
        this.showChooseLamp();
    }

    chooseCouch2() {
        // cc.audioEngine.play(this.clickButton, false, 1);
        this.couchManager.chooseCouch2();
        this.couchNode.selectedNode();
        this.noticeNode.disappear();
        this.showChooseLamp();
    }

    chooseCouch3() {
        // cc.audioEngine.play(this.clickButton, false, 1);
        this.couchManager.chooseCouch3();
        this.couchNode.selectedNode();
        this.noticeNode.disappear();
        this.showChooseLamp();
    }

    showChooseLamp() {
        this.scheduleOnce(() => {
            this.noticeNode.setText("tree_text_kr");
            this.noticeNode.appear();
            this.lampNode.appear();
        }, 0.5);
    }

    chooseLamp1() {
        // cc.audioEngine.play(this.clickButton, false, 1);
        this.lampManager.chooseLamp1();
        this.lampManager1.chooseLamp1();
        this.lampNode.selectedNode();
        this.noticeNode.disappear();
        this.showChooseWallDecor();
    }
    chooseLamp2() {
        // cc.audioEngine.play(this.clickButton, false, 1);
        this.lampManager.chooseLamp2();
        this.lampManager1.chooseLamp2();
        this.lampNode.selectedNode();
        this.noticeNode.disappear();
        this.showChooseWallDecor();
    }
    chooseLamp3() {
        // cc.audioEngine.play(this.clickButton, false, 1);
        this.lampManager.chooseLamp3();
        this.lampManager1.chooseLamp3();
        this.lampNode.selectedNode();
        this.noticeNode.disappear();
        this.showChooseWallDecor();
    }

    showChooseWallDecor() {
        this.scheduleOnce(() => {
            this.noticeNode.setText("sheft_text_kr");
            this.noticeNode.appear();
            this.wallDecorNode.appear();
        }, 0.5);
    }
    chooseWall1() {
        // cc.audioEngine.play(this.clickButton, false, 1);
        this.wallDecorManager.chooseDecor1();
        // this.wallDecorManager1.chooseDecor1();
        this.wallDecorNode.selectedNode();
        this.noticeNode.disappear();
        this.showEndGame();
    }
    chooseWall2() {
        // cc.audioEngine.play(this.clickButton, false, 1);
        this.wallDecorManager.chooseDecor2();
        // this.wallDecorManager1.chooseDecor2();
        this.wallDecorNode.selectedNode();
        this.noticeNode.disappear();
        this.showEndGame();
    }
    chooseWall3() {
        // cc.audioEngine.play(this.clickButton, false, 1);
        this.wallDecorManager.chooseDecor3();
        // this.wallDecorManager1.chooseDecor3();
        this.wallDecorNode.selectedNode();
        this.noticeNode.disappear();
        this.showEndGame();
    }
    showEndGame() {
        this.scheduleOnce(() => {
            // cc.audioEngine.play(this.endSound, false, 1);
            this.endGameNode.active = true;
            this.endGameAni.callScheduleShowAnimation();
        }, 0.5);
    }
}
