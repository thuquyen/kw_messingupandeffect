

const { ccclass, property } = cc._decorator;

@ccclass
export default class ShowCorrectItem extends cc.Component {
    @property(cc.Node)
    Item: cc.Node = null;
    @property(cc.Node)
    DoneItem: cc.Node = null;


    start() {

    }
    StartButton() {
        this.Item.opacity = 0;
        this.DoneItem.opacity = 255;
    }

    // update (dt) {}
}
