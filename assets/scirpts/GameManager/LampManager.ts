import LampItem from "./LampItem";

const { ccclass, property } = cc._decorator;

@ccclass
export default class LampManager extends cc.Component {

    @property(LampItem)
    lamp1: LampItem = null;
    @property(LampItem)
    lamp2: LampItem = null;
    @property(LampItem)
    lamp3: LampItem = null;


    chooseLamp1() {
        this.lamp1.playAnimation();
    }
    chooseLamp2() {
        this.lamp2.playAnimation();
    }
    chooseLamp3() {
        this.lamp3.playAnimation();
    }
}
