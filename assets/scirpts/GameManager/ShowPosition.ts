

const { ccclass, property } = cc._decorator;

@ccclass
export default class ShowPosition extends cc.Component {

    @property(cc.Node)
    posItem1: cc.Node[] = [];

    @property(cc.Node)
    posItem2: cc.Node[] = [];

    start() {

    }

    showPosItem1(){
        for(let i = 0; i <this.posItem1.length; i++){
            this.posItem1[i].opacity=255;
        }
    }
    showPosItem2(){
        for(let i = 0; i< this.posItem2.length;i++){
            this.posItem2[i].opacity= 255;
        }
    }
    hidePosItem1(){
        for(let i=0; i<this.posItem1.length;i++){
            this.posItem1[i].opacity =0;
        }
    }
    hidePosItem2(){
        for(let i=0; i<this.posItem2.length;i++){
            this.posItem2[i].opacity=0;
        }
    }
    // update (dt) {}
}
