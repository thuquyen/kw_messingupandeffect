import Item2 from "./Item2";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Item2Manager extends cc.Component {

    @property(Item2)
    item21: Item2 = null;
    @property(Item2)
    item22: Item2 = null;
    @property(Item2)
    item23: Item2 = null;

    chooseItem21() {
        this.item21.playAnimation();
    }

    chooseItem22() {
        this.item22.playAnimation();
    }

    chooseItem23() {
        this.item23.playAnimation();
    }
    hide(){
        this.item21.hideNode();
        this.item22.hideNode();
        this.item23.hideNode();
    }
}
