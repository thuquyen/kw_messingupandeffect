import WallDecorItem from "./WallDecorItem";

const { ccclass, property } = cc._decorator;

@ccclass
export default class WallDecorManager extends cc.Component {
    @property(WallDecorItem)
    decor1: WallDecorItem = null;
    @property(WallDecorItem)
    decor2: WallDecorItem = null;
    @property(WallDecorItem)
    decor3: WallDecorItem = null;

    chooseDecor1() {
        this.decor1.playAnimation();
    }
    chooseDecor2() {
        this.decor2.playAnimation();
    }
    chooseDecor3() {
        this.decor3.playAnimation();
    }
}
