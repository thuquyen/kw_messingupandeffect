const { ccclass, property } = cc._decorator;

@ccclass
export default class LampItem extends cc.Component {

    @property(cc.Animation)
    anims: cc.Animation = null;

    playAnimation() {
        this.anims.play();
    }
}
