
const { ccclass, property } = cc._decorator;

@ccclass
export default class WallDecorItem extends cc.Component {

    @property(cc.Animation)
    clockAni: cc.Animation = null;
    playAnimation() {
        this.clockAni.play();
    }
}
