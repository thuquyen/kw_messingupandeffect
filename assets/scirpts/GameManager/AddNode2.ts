
const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Animation)
    add1: cc.Animation = null;

    @property(cc.Animation)
    add2: cc.Animation = null;


    @property(Number)
    delayTime: number = 0.1;

    @property(cc.Animation)
    appearEff: cc.Animation[] = [];

    playAnimation() {
        this.add1.play();
        this.add2.play();

        for (let i = 0; i < this.appearEff.length; i++) {
            this.appearEff[i].node.active = true;
        }
        for (let i = 0; i < this.appearEff.length; i++) {
            this.appearEff[i].play();
        }
    }
    hideNode() {
        this.add1.node.opacity = 0;
        this.add2.node.opacity = 0;
  
    }
    // update (dt) {}
}
