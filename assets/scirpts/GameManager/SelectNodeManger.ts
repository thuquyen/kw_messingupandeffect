import HandAction from "./HandAction";
import Global from "../Utils/Global";

const { ccclass, property } = cc._decorator;

@ccclass
export default class SelectNodeManager extends cc.Component {

    @property(cc.Animation)
    selectAni: cc.Animation = null;
    @property(cc.Animation)
    btnChoose1Ani: cc.Animation = null;
    @property(cc.Animation)
    btnChoose2Ani: cc.Animation = null;
    @property(cc.Animation)
    btnChoose3Ani: cc.Animation = null;
    @property([cc.Node])
    handChooseItem: cc.Node[] = [];
    private handCount = 0;

    start() {
        this.callScheduleShowHandAction();
    }

    showHandAction() {
        if (!Global.isHideHand) {
            if (this.handCount > 2)
                this.handCount = 0;
            this.handChooseItem[this.handCount].active = true;
            this.handChooseItem[this.handCount].getComponent(HandAction).handSchedule();
            for (let i = 0; i < 3; i++) {
                if (i !== this.handCount) {
                    this.handChooseItem[i].active = false;
                }
            }
            this.handCount++;
        }
    }

    callScheduleShowHandAction() {
        this.schedule(this.showHandAction, 2, cc.macro.REPEAT_FOREVER, 0);
    }

    appear() {
        this.node.setPosition(cc.Vec2.ZERO);
        this.btnChoose1Ani.play();
        this.scheduleOnce(() => {
            this.btnChoose2Ani.play();
        }, 0.1);
        this.scheduleOnce(() => {
            this.btnChoose3Ani.play();
        }, 0.2);
    }
    selectedNode() {
        this.selectAni.play();
    }
}
