import CouchItem from "./CouchItem";
import Item1 from "./Item1";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Item1Manager extends cc.Component {

    @property(CouchItem)
    item11: CouchItem = null;
    @property(CouchItem)
    item12: CouchItem = null;
    @property(CouchItem)
    item13: CouchItem = null;

    chooseItem11() {
        this.item11.playAnimation();
    }

    chooseItem12() {
        this.item12.playAnimation();
    }

    chooseItem13() {
        this.item13.playAnimation();
    }

    hide(){
        this.item11.hideNode();
        this.item12.hideNode();
        this.item13.hideNode();
    }
}
