const { ccclass, property } = cc._decorator;

@ccclass
export default class Item1 extends cc.Component {

    @property(cc.Animation)
    anims: cc.Animation = null;

    playAnimation() {
        this.anims.play();
    }
}
