import Item3 from "./Item3";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Item3Manager extends cc.Component {

    @property(Item3)
    item31: Item3 = null;
    @property(Item3)
    item32: Item3 = null;
    @property(Item3)
    item33: Item3 = null;

    chooseItem31() {
        this.item31.playAnimation();
    }

    chooseItem32() {
        this.item32.playAnimation();
    }

    chooseItem33() {
        this.item33.playAnimation();
    }
}
