import CouchItem from "./CouchItem";

const { ccclass, property } = cc._decorator;

@ccclass
export default class CouchManager extends cc.Component {

    @property(CouchItem)
    couch1: CouchItem = null;
    @property(CouchItem)
    couch2: CouchItem = null;
    @property(CouchItem)
    couch3: CouchItem = null;

    chooseCouch1() {
        this.couch1.playAnimation();
    }

    chooseCouch2() {
        this.couch2.playAnimation();
    }

    chooseCouch3() {
        this.couch3.playAnimation();
    }
}
