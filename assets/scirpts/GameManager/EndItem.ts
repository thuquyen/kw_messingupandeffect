const { ccclass, property } = cc._decorator;

@ccclass
export default class CouchItem extends cc.Component {


    @property(cc.Animation)
    node1: cc.Animation = null;
    @property(cc.Animation)
    node23: cc.Animation = null;
    @property(cc.Animation)
    node4: cc.Animation = null;
    @property(cc.Animation)
    node5: cc.Animation = null;

    @property(cc.Animation)
    node7: cc.Animation =null;
    @property(Number)
    delayTime: number = 0.1;

    start() {
            this.node1.play();
        this.scheduleOnce(() => {
            this.node23.play();
        }, this.delayTime);

        this.scheduleOnce(() => {
            this.node4.play();
        }, this.delayTime);

        this.scheduleOnce(() => {
            this.node5.play();
        }, this.delayTime*2);


        this.scheduleOnce(() => {
            this.node7.play();
        }, this.delayTime*2);
    }
}
