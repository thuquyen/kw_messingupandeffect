
const { ccclass, property } = cc._decorator;

@ccclass
export default class NoticeManagerLocalize extends cc.Component {

    @property(cc.Sprite)
    spriteF: cc.Sprite = null;
    @property(cc.Animation)
    noticeAni: cc.Animation = null;
    @property([cc.SpriteFrame])
    spriteFrame: cc.SpriteFrame[] = [];
    // @property()
    // public numberNotice: number = 0;


    appear() {
        this.noticeAni.play("notice2");
    }

    disappear() {
        this.noticeAni.play("notice2out");
    }

    setText(mess) {
        if (mess === "Item2_txt_kr")
            this.spriteF.spriteFrame = this.spriteFrame[0];
    }
}
