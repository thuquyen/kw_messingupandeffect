const { ccclass, property } = cc._decorator;

@ccclass
export default class NoticeManager extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;
    @property(cc.Animation)
    noticeAni: cc.Animation = null;
    

    appear() {
        this.noticeAni.play("noticeAction");
    }

    disappear() {
        this.noticeAni.play("noteActionOut");
    }

    setText(mess) {
        this.label.string = mess;
    }
}
