
const { ccclass, property } = cc._decorator;

@ccclass
export default class NoticeManagerLocalize extends cc.Component {

    @property(cc.Sprite)
    spriteF: cc.Sprite = null;
    @property(cc.Animation)
    noticeAni: cc.Animation = null;
    @property([cc.SpriteFrame])
    spriteFrame: cc.SpriteFrame[] = [];



    appear() {
        this.noticeAni.play("notice2");
    }

    disappear() {
        this.noticeAni.play("note2out");
    }

    setText(mess) {
        if (mess === "tree_text_kr")
            this.spriteF.spriteFrame = this.spriteFrame[0];
        else
            this.spriteF.spriteFrame = this.spriteFrame[1];
        // this.label.string = mess;
    }
}
