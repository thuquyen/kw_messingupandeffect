const { ccclass, property } = cc._decorator;

@ccclass
export default class EndGame extends cc.Component {

    @property([cc.Animation])
    btnAnis: cc.Animation[] = [];
    private count = 0;

    // onLoad () {}

    start() {
        this.callScheduleShowAnimation();
    }

    showAnimation() {
        if (this.count > 1)
            this.count = 0;
        this.btnAnis[this.count].play();
        for (let i = 0; i < 2; i++) {
            if (i !== this.count)
                this.btnAnis[i].stop();
        }
        this.count++;
    }

    callScheduleShowAnimation() {
        this.schedule(this.showAnimation, 1, cc.macro.REPEAT_FOREVER, 0.2);
    }

    // update (dt) {}
}
