const { ccclass, property } = cc._decorator;

@ccclass
export default class HandAction extends cc.Component {
    @property(cc.Animation)
    handAnims: cc.Animation = null;
    
    handSchedule() {
        this.handAnims.play("handFadeIn", 0.2);
        this.scheduleOnce(() => {
            this.handAnims.play("handAction");
            this.scheduleOnce(() => {
                this.handAnims.play("handFadeOut");
            }, 1.2)
        }, 0);
    }
}
