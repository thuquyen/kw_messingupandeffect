import AddNode1 from "./AddNode1"
const { ccclass, property } = cc._decorator;

@ccclass
export default class CouchItem extends cc.Component {

    @property(cc.Animation)
    sofa1Ani: cc.Animation = null;
    @property(cc.Animation)
    tableAni: cc.Animation = null;
    @property(cc.Animation)
    sofabackAni: cc.Animation = null;
    @property(cc.Animation)
    carpetAni: cc.Animation = null;
    // @property(cc.Animation)
    // lamp: cc.Animation = null;
    @property(Number)
    delayTime: number = 0.1;
    @property(AddNode1)
    AddNode1: AddNode1 = null;
    @property(cc.Animation)
    AppearEff: cc.Animation = null;

    playAnimation() {
        this.AppearEff.node.active = true;
        this.sofa1Ani.play();
        this.scheduleOnce(() => {
            this.sofabackAni.play();
        }, this.delayTime)
        this.scheduleOnce(() => {
            // this.lamp.play();
            this.carpetAni.play();
            this.tableAni.play();
        }, this.delayTime * 2);
        this.scheduleOnce(() => {
            this.AddNode1.playAnimation();
        }, this.delayTime * 6.5)
        this.AppearEff.play();
    }
    hideNode() {
        this.sofa1Ani.node.opacity = 0;
        this.tableAni.node.opacity = 0;
        this.sofabackAni.node.opacity = 0;
        this.carpetAni.node.opacity = 0;
        // this.lamp.node.opacity = 0;
    }
}
