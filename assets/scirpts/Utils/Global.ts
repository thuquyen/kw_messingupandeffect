
interface Global {
    touchPos: cc.Vec2,
    isHideHand: boolean,
}

let Global: Global = {
    touchPos: null,
    isHideHand: false,
}

export default Global;